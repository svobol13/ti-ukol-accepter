package cz.svobol.ti;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class InputStreamParser implements Parser {

    private InputStream is;
    private Variable startVariable;

    public void parse(Object is) {
        this.is = (InputStream) is;
        startVariable.process();
    }

    public Character pop() {
        int value = -1;
        try {
            value = is.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return value < 0 ? null : (char) value;
    }

    public Character peek() {
        is.mark(0);
        int value = -1;
        try {
            value = is.read();
            is.reset();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return value < 0 ? null : (char) value;
    }

    public void expectOnTop(char expected) {
        Character actual = popSymbol();
        if (actual != expected) {
            fail("Expected value is " + expected + " but was " + actual + ".");
        }
    }

    public void fail(String reason) {
        throw new ParseErrorException(reason);
    }

    public void setStartingVariable(Variable startVariable) {
        this.startVariable = startVariable;
    }

    public void setInputStream(InputStream is) {
        this.is = is;
    }

    public Character popSymbol() {
        Character c = peek();
        while (c != null && Character.isSpaceChar(c)) {
            pop();
            c = peek();
        }
        return pop();
    }

    public Character peekSymbol() {
        Character c = peek();
        while (c != null && Character.isSpaceChar(c)) {
            pop();
            c = peek();
        }
        return c;
    }

    public Variable getStartVariable() {
        return this.startVariable;
    }

    public void expectOneOf(List<Character> follow) {
        Character ch = peekSymbol();
        if (!follow.contains(ch)) {
            fail("One of " + follow.toString() + " expected, but " + ch + " found.");
        }
    }

}

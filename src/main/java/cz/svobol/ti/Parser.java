package cz.svobol.ti;

import java.io.InputStream;
import java.util.List;

/**
 *
 * @author Lukas Svoboda
 */
public interface Parser {

    void parse(Object o);

    Character pop();

    Character popSymbol();

    Character peek();

    Character peekSymbol();

    /**
     * Checks whether expected value is on top of stack.
     * @param Variable expected to be popped out.
     */
    void expectOnTop(char expected);

    void fail(String reason);

    void setStartingVariable(Variable s);

    Variable getStartVariable();

    void setInputStream(InputStream is);

    void expectOneOf(List<Character> follow);
}

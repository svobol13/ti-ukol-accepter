package cz.svobol.ti;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Class modeling a variable.
 *
 * @author Lukas Svoboda
 */
public class Variable {

    private static final String MULTIPLE_RULES_FOUND = "More rules starting with this character.";

    private Parser parser;
    public String name;

    public Variable(Parser parser) {
        this.parser = parser;
    }

    public Variable(Parser parser, String name) {
        this(parser);
        this.name = name;
    }

    List<Rule> rules = new ArrayList<Rule>();

    public void addRule(Object... output) {
        rules.add(new Rule(this, output));
    }

    public void process() {
        // find rule
        List<Rule> candidateRules = new LinkedList<Rule>();
        Character symbol = parser.peekSymbol();
        for (Rule rule : rules) {
            if (rule.first().contains(symbol)) {
                candidateRules.add(rule);
            }
        }

        if (candidateRules.size() != 1) {
            parser.fail("There was found " + candidateRules.size() + " posible transition rules, " +
                    "but only allowed count is 1.");
        }

        Rule rule = candidateRules.iterator().next();
        for (Object o : rule.getOutput()) {
            if (o instanceof Character) {
                parser.expectOnTop((Character) o);
            } else if (o == null) {
                parser.expectOneOf(rule.getVariable().follow());
            } else if (o instanceof Variable) {
                ((Variable) o).process();
            }
        }
    }

    /**
     * @return Array of all possible first symbols.
     */
    public List<Character> first() {
        List<Character> first = new LinkedList<Character>();
        // merge first from all rules
        for (Rule rule : rules) {
            first.addAll(rule.first());
        }
        return first;
    }

    /**
     * Returns a rule by the given firstSymbol.
     *
     * @param firstSymbol Character to determine the rule.
     * @return
     */
    public Rule getRule(Character firstSymbol) {
        List<Rule> result = new LinkedList<Rule>();
        for (Rule rule : this.rules) {
            if (rule.first().contains(firstSymbol)) {
                result.add(rule);
            }
        }
        if (result.size() > 1) {
            parser.fail(MULTIPLE_RULES_FOUND);
        }
        if (result.size() == 1) {
            return result.iterator().next();
        }
        return null;
    }

    /**
     * Finds all rules that contains given variable in output.
     * @param variable
     * @return
     */
    public Set<Rule> getRules(Variable variable, Set<Variable> antiLoopStack) {
        Set<Rule> result = new LinkedHashSet<Rule>();
        if (antiLoopStack == null) {
            antiLoopStack = new LinkedHashSet<Variable>();
        }
        for (Rule rule : rules) {
            for (int i = 0; i < rule.getOutput().length; i++) {
                Object o = rule.getOutput()[i];
                // find any variable in the output
                if (o instanceof Variable) {
                    // found occurence of the variable
                    if (o.equals(variable)) {
                        result.add(rule);
                    }
                    if (antiLoopStack.contains(o)) {
                        continue;
                    }
                    antiLoopStack.add((Variable) o);
                    result.addAll(((Variable)o).getRules(variable, antiLoopStack));
                }
            }
        }
        return result;
    }

    public List<Rule> getRules() {
        return rules;
    }

    /**
     * @return Array of all possible follow symbols.
     */
    public List<Character> follow() {
        Set<Rule> rules = new LinkedHashSet<Rule>();
        Set<Character> followChar = new LinkedHashSet<Character>();
        Set<Variable> followVar = new LinkedHashSet<Variable>();

        for (Rule rule : parser.getStartVariable().getRules(this, null)) {
            for (int i = 0; i < rule.getOutput().length; i++) {
                Object obj = rule.getOutput()[i];
                if (obj.equals(this)) {
                    if (i < rule.getOutput().length - 1) {
                        Object next = rule.getOutput()[i+1];
                        if (next instanceof Variable) {
                            followVar.add((Variable) next);
                        } else if (next instanceof Character) {
                            followChar.add((Character) next);
                        }
                    } else if (!rule.getVariable().equals(this)) {
                        followChar.addAll(rule.getVariable().follow());
                    }
                }
            }
        }

        for (Variable var : followVar) {
            followChar.addAll(var.first());
        }

        return new ArrayList<Character>(followChar);
    }

    public Parser getParser() {
        return parser;
    }
}

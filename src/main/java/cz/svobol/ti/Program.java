package cz.svobol.ti;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class Program {

    public static final Character LAMBDA = null;
    private static InputStream is = new BufferedInputStream(System.in);
    private static Parser parser = new InputStreamParser();

    public static final Variable E = new Variable(parser, "E");
    public static final Variable O = new Variable(parser, "O");
    public static final Variable Oa = new Variable(parser, "Oa");
    public static final Variable A = new Variable(parser, "A");
    public static final Variable Aa = new Variable(parser, "Aa");
    public static final Variable N = new Variable(parser, "N");
    public static final Variable Na = new Variable(parser, "Na");
    public static final Variable Z = new Variable(parser, "Z");
    public static final Variable V = new Variable(parser, "V");

    static {
        parser.setStartingVariable(E);

        E.addRule(O, '#');
        O.addRule(A, Oa);
        Oa.addRule('|', A, Oa);
        Oa.addRule(LAMBDA);
        A.addRule(N, Aa);
        Aa.addRule('&', N, Aa);
        Aa.addRule(LAMBDA);
        N.addRule(Z);
        N.addRule('~', N);
        Z.addRule('(', O, ')');
        Z.addRule(V);
        V.addRule('0');
        V.addRule('1');
    }

    public static void main(String[] args) throws UnsupportedEncodingException {
        try {
            parser.parse(new ByteArrayInputStream(args[0].getBytes("UTF-8")));
            System.out.println("A");
        } catch (ParseErrorException e) {
            System.out.println("N");
        }
    }

    public static Parser getParser() {
        return parser;
    }

    private static void test(Character... ch) {
        Character c = ch[0];
        System.out.println("�sdf");
    }

}

package cz.svobol.ti;

import java.util.LinkedList;
import java.util.List;

/**
 * Class that models transition from variable
 *
 * @author Lukas Svoboda
 *
 */
public class Rule {

    private Variable variable;
    private Object[] output;

    public Rule(final Variable input, final Object[] output) {
        checkOutput(output);
        this.variable = input;
        this.output = output;
    }

    private void checkOutput(Object[] output) {
        for (Object o : output) {
            if (!(o instanceof Character || o instanceof Variable || o instanceof Matcher || o == null)) {
                throw new IllegalArgumentException("Output object must be isntance of Character, Variable, Matcher or null");
            }
        }
    }

    public void process() {
        for (Object o : output) {
            process(o);
        }
    }

    private void process(Object o) {
        if (o instanceof Character) {
            Character ch = (Character) o;
            variable.getParser().expectOnTop(ch);
        } else if (o instanceof Variable) {
            Variable var = (Variable) o;
            var.process();
        } else {
            Matcher m = (Matcher) o;
            if (!m.match(variable.getParser().popSymbol())) {
                variable.getParser().fail("Symbol was not expected by matcher.");
            }
        }
    }

    public Object[] getOutput() {
        return this.output;
    }

    public Variable getVariable() {
        return this.variable;
    }

    public List<Character> first() {
        List<Character> first = new LinkedList<Character>();
        if (output[0] instanceof Character) {
            first.add((Character) output[0]);
        } else if (output[0] == null) {
            first.addAll(variable.follow());
        } else if (output[0] instanceof Matcher) {
            throw new RuntimeException("Sorry matcher is not supported yet");
        } else if (output[0] instanceof Variable) {
            Variable v = (Variable) output[0];
            first.addAll(v.first());
        } else {
            throw new RuntimeException("Unexpected output value type.");
        }
        return first;
    }

    /**
     * Return
     * @param variable
     * @return Character|Variable|null
     */
    public Object getFollowObject(Variable variable) {
        for (int i = 0; i < output.length; i++) {
            if (variable.equals(output[i]) && i != output.length) {
                return output[i++];
            }
        }
        return null;
    }
}

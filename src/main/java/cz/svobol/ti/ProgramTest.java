package cz.svobol.ti;

import static junit.framework.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;

import org.junit.Test;

public class ProgramTest {

    @Test
    public void testMain() {
        String[][] testMatrix = new String[][] {
            {"A", "0|1#"},
            {"A", "0&0#"},
            {"A", "0 & (0)#"},
            {"A", "0 | ~(0 & 1)#"},
            {"A", "(0 | ~(0 & 1))#"},
            {"A", "~(0 | ~(0 & 1))#"},
            {"A", "~~((0) | ~(0 & 1))#"},
            {"A", "0#"},
            {"A", "(1)#"},

            {"N", "0 | 1"},
            {"N", "0~&0#"},
            {"N", "0 1& (0)#"},
            {"N", "0 | 1 ~(0 & 1)#"},
            {"N", "(| 0 | ~(0 & 1))#"},
            {"N", "1~(0 | ~(0 & 1))#"},
            {"N", "~(|(0) | ~(0 & 1))#"},
            {"N", "0|#"},
            {"N", "1)#"},
        };

        testMatrix(testMatrix);
    }

    private void testMatrix(String[][] testMatrix) {
        String s = "";
        for (int line = 0; line < testMatrix.length; line++) {
            String v = "";
            try {
                Program.getParser().parse(new ByteArrayInputStream(testMatrix[line][1].getBytes("UTF-8")));
                v = "A";
            } catch (ParseErrorException e) {
                v = "N";
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            assertEquals(testMatrix[line][0], v);
        }
    }
}

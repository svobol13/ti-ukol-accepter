package cz.svobol.ti;

public interface Matcher {
    boolean match(Character symbol);
}

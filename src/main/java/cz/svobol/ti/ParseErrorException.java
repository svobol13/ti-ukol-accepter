package cz.svobol.ti;

public class ParseErrorException extends RuntimeException {

    public ParseErrorException(String reason) {
        super(reason);
    }

    private static final long serialVersionUID = -4402597202000496904L;

}

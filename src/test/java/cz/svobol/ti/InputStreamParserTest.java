package cz.svobol.ti;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.*;

public class InputStreamParserTest {

    private Parser parser;

    @Before
    public void init() {
        parser = new InputStreamParser();
    }

    @Test
    public void testPopPeek() throws UnsupportedEncodingException {
        parser.setInputStream(new ByteArrayInputStream(" 0".getBytes("UTF-8")));
        assertFalse('0' == parser.peek());
        assertFalse('0' == parser.peek());
        assertTrue('0' == parser.peekSymbol());
        assertTrue('0' == parser.popSymbol());
    }

}

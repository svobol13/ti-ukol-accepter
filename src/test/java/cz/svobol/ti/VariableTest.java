package cz.svobol.ti;

import static cz.svobol.ti.Program.A;
import static cz.svobol.ti.Program.Aa;
import static cz.svobol.ti.Program.E;
import static cz.svobol.ti.Program.N;
import static cz.svobol.ti.Program.O;
import static cz.svobol.ti.Program.Oa;
import static cz.svobol.ti.Program.V;
import static cz.svobol.ti.Program.Z;

import org.junit.Test;

public class VariableTest {

    @Test
    public void testFirstFollow() {
        testVariable(E, charr('(', '0', '1', '~'), charr());
        testVariable(O, charr('(', '0', '1', '~'), charr('#', ')'));
        testVariable(Oa, charr('|', '#', ')'), charr('#', ')'));
        testVariable(A, charr('(', '0', '1', '~'), charr('|', '#', ')'));
        testVariable(Aa, charr('&', '|', '#', ')'), charr('|', '#', ')'));
        testVariable(N, charr('(', '0', '1', '~'), charr('&', '|', '#', ')'));
        testVariable(Z, charr('(', '0', '1'), charr('&', '|', '#', ')'));
        testVariable(V, charr('0', '1'), charr('&', '|', '#', ')'));
    }

    private void testVariable(Variable variable, Character[] firstExpected, Character[] followExpected) {
        Character[] firstActual = variable.first().toArray(new Character[variable.first().size()]);
        Character[] followActual = variable.follow().toArray(new Character[variable.follow().size()]);

        compare(firstExpected, firstActual);
        compare(followExpected, followActual);
    }



    private void compare(Character[] expected, Character[] actual) {
        if (expected.length != actual.length) {
            fail("Lengths of array differs.", expected, actual);
        }
        for (int i = 0; i < expected.length; i++) {
            if (expected[i] != actual[i]) {
                fail("Expected " + expected[i] + " but " + actual[i] + " found.", expected, actual);
            }
        }
    }

    private void fail(String message, Character[] expected, Character[] actual) {
        throw new RuntimeException(message + "Expected: " + charArrayToString(expected) + " Actual " + charArrayToString(actual));
    }

    private String charArrayToString(Character[] array) {
        String s = "[";
        for (Character ch : array) {
            if (ch == null) s += " null";
            else s += " " + ch + ",";
        }
        s += "]";
        return s;
    }

    private Character[] charr(Character... chars) {
        return chars;
    }
}
